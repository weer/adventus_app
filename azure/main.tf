# Configure the Microsoft Azure Provider
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}

provider "azurerm" {
    subscription_id = var.azure_subscription_id
    client_id       = var.azure_client_id
    client_secret   = var.azure_client_secret
    tenant_id       = var.azure_tenant_id
    features {}
}

# Create password for VM
resource "random_string" "adventus_vm_password" {
  length      = 20
  special     = false
  min_upper   = 5
  min_lower   = 5
  min_numeric = 3
}

# Create a resource group if it doesn't exist
resource "azurerm_resource_group" "adventusrg" {
    name     = "${lower(var.custcode)}-rg"
    location = var.region

    tags = {
        name     = "${lower(var.custcode)}-rg"
    }
}

# Create virtual network
resource "azurerm_virtual_network" "adventusnetwork" {
    name                = "${lower(var.custcode)}-Vnet"
    address_space       = ["10.0.0.0/16"]
    location            = var.region
    resource_group_name = azurerm_resource_group.adventusrg.name

    tags = {
        name     = "${lower(var.custcode)}-vnet"
    }
}

# Create subnet
resource "azurerm_subnet" "adventussubnet" {
    name                 = "${lower(var.custcode)}-Subnet"
    resource_group_name  = azurerm_resource_group.adventusrg.name
    virtual_network_name = azurerm_virtual_network.adventusnetwork.name
    address_prefixes       = ["10.0.1.0/24"]
}

# Create public IPs
resource "azurerm_public_ip" "adventuspublicip" {
    name                         = "${lower(var.custcode)}-PublicIP"
    location                     = var.region
    resource_group_name          = azurerm_resource_group.adventusrg.name
    allocation_method            = "Static"

    tags = {
        name     = "${lower(var.custcode)}-PIP"
    }
}

#create a data to recicve ip
data "azurerm_public_ip" "adventuspublicip" {
  name                = azurerm_public_ip.adventuspublicip.name
  resource_group_name = azurerm_resource_group.adventusrg.name
}

output "vm_ip" {
  value = data.azurerm_public_ip.adventuspublicip.ip_address
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "adventusnsg" {
    name                = "${lower(var.custcode)}-NetworkSecurityGroup"
    location            = var.region
    resource_group_name = azurerm_resource_group.adventusrg.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    security_rule {
        name                       = "http"
        priority                   = 1002
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "8000"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        name     = "${lower(var.custcode)}-nsg"
    }
}

# Create network interface
resource "azurerm_network_interface" "adventusnic" {
    name                      = "${lower(var.custcode)}-NIC"
    location                  = var.region
    resource_group_name       = azurerm_resource_group.adventusrg.name

    ip_configuration {
        name                          = "${lower(var.custcode)}-NicConfiguration"
        subnet_id                     = azurerm_subnet.adventussubnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.adventuspublicip.id
    }

    tags = {
        name     = "${lower(var.custcode)}-nic"
    }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "example" {
    network_interface_id      = azurerm_network_interface.adventusnic.id
    network_security_group_id = azurerm_network_security_group.adventusnsg.id
}

# Create virtual machine
resource "azurerm_linux_virtual_machine" "adventusvm" {
    name                  = "${lower(var.custcode)}-VM"
    location              = var.region
    resource_group_name   = azurerm_resource_group.adventusrg.name
    network_interface_ids = [azurerm_network_interface.adventusnic.id]
    size                  = "Standard_D2s_v3"

    os_disk {
        name              = "${lower(var.custcode)}OsDisk"
        caching           = "ReadWrite"
        storage_account_type = "Premium_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    computer_name  = "${lower(var.custcode)}-vm"
    admin_username = "adventus"
    admin_password = random_string.adventus_vm_password.result
    disable_password_authentication = false

    provisioner "local-exec" {
        command     = "echo random_string.adventus_vm_password.result"
    }

    provisioner "remote-exec" {
        connection {
            host     = data.azurerm_public_ip.adventuspublicip.ip_address
            type     = "ssh"
            user     = "adventus"
            password = random_string.adventus_vm_password.result
        }

        inline = [
            "mkdir -p /home/adventus/webapp",
            "sudo apt-get update",
            "sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common",
            "curl -fsSL https://get.docker.com -o get-docker.sh",
            "sh get-docker.sh",
            "sudo curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose",
            "sudo chmod +x /usr/local/bin/docker-compose",
            "sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose",
            "cd /home/adventus/webapp",
            "git clone https://weer:Sc%2F2005%2F5874@gitlab.com/weer/adventus_app.git",
            "cd adventus_app/adventus-app",
            "sudo groupadd docker",
            "sudo usermod -aG docker $(whoami)",
            "sudo docker-compose build",
            "sudo docker-compose up -d",
            "sudo docker-compose exec web php artisan key:generate",
        ]
    }

    tags = {
        name     = "${lower(var.custcode)}-vm"
    }
}
