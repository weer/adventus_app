<b>Tech stack</b>
1. Terraform
2. Docker
3. Docker-compose
4. Bash scripts
5. Azure
6. GitLab and GitLab pipelines

<b>Reasons why choose given tech stack</b>
1. <b>Terraform</b>, is an open-source solution that enables the development of infrastructure-as-a-code. It is a powerful tool that helps in IT operations, provision, upgrade, and maintain infrastructure. This is a fully JSON compatible language that helps the DevOps professionals to define the infrastructure-as-a-code for any of the cloud flatform.

2. <b>Docker</b>, is an open sourse OS virtualized software platform that allows IT organizations to easily create, deploy, and run applications in Docker containers, which have all the dependencies within them. The container itself is really just a very lightweight package that has all the instructions and dependencies—such as frameworks, libraries, and bins—within it.

 - Return on Investment and Cost Savings
 - Rapid Deployment
 - Simplicity and Faster Configurations
 - CI Efficiency

 3. <b>Bash scripts</b>, well support for Linux environments

 4. <b>Azure</b>, is an industry-leading cloud service offering some of the services Azure provides are infrastructure as service, compute services, databases, web apps hosting, API management, analytics, mobile apps and application integrations, and DevOps. Azure being a fast-growing cloud vendor helps businesses to faster the business process and provides the reliability of the It infrastructures.

 5. <b>GitLab and GitLab pipelines</b>, GitLab supports the full DevOps lifecycle in one environment. Which means that we can use GitLab CI/CD for each of the continuous practices. Without integrating any third-party applications and tools.

<b>Deployment instructions</b>
1. Create a Azure account and create a SPN account for terraform to use.
2. Create a GitLab project.
3. Clone this project and commit the code to the GitLab project.
4. Go to GitLab project setting and select CI/CD, under variable add the following variables (SPN and subscription details)
<img src="var.png" width="500" /><br>
5. Create a GitLab token under Pipeline triggers section in project settings

<b>How to run tests</b>

Run the following curl command to trigger the pipeline in any command line tool such as GitBash


curl -X POST &#92;<br>
-F token=replace the gitlab token here &#92;<br>
-F ref=replace the gitlab branch name here &#92;<br>
-F "variables[TF_VAR_custcode]=replace the customer code here ex: adventus" &#92;<br>
-F "variables[TF_VAR_region]=replace the azure region here ex: eastus" &#92;<br>
-F "variables[trigger_now]=yes" &#92; // this is always 'yes' <br>
-F "variables[azure_subscription_id]=replace the azure subscription id here" &#92;<br>
https://gitlab.com/api/v4/projects/31673336/trigger/pipeline  // this endpoint api url can be find under Pipeline triggers section in project settings

ex:

curl -X POST &#92;<br>
-F token=eb4adsf436df2fe6836d36d12 &#92;<br>
-F ref=main &#92;<br>
-F "variables[TF_VAR_custcode]=adventus" &#92;<br>
-F "variables[TF_VAR_region]=eastus" &#92;<br>
-F "variables[trigger_now]=yes" &#92;<br>
-F "variables[azure_subscription_id]=xxxaaa-7ddd-sxe3-ree45-cf42e9968e32" &#92;<br>
https://gitlab.com/api/v4/projects/31673336/trigger/pipeline

Then it will start to run the pipeline. After successfully run the pipeline, it will create azure infrastructures and deploy the Laravel app in to a Docker container.

Then go to azure portal and select the VM public IP address and browse the web app as follows.

http://VM public IP address:8000

ex: http://20.124.29.255:8000/  // this is my app
